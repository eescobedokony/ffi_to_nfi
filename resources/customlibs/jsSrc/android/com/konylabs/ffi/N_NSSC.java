package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.bayteq.smartcheckapi.WrapperFFI;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_NSSC extends JSLibrary {

 
 
	public static final String NSSCF = "NSSCF";
 
	String[] methods = { NSSCF };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_NSSC(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 com.konylabs.vm.Function fun0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 fun0 = (com.konylabs.vm.Function)params[0];
 }
 java.lang.String str0 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 str0 = (java.lang.String)params[1];
 }
 ret = this.NSSCF( fun0, str0 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "NSSC";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] NSSCF( com.konylabs.vm.Function inputKey0, java.lang.String inputKey1 ){
 
		Object[] ret = null;
 com.bayteq.smartcheckapi.WrapperFFI.startSmartCheck( (com.konylabs.vm.Function)inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
